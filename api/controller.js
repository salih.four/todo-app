const { taskModel, listModel } = require("./models");

async function deleteList(req, res, next) {
  const { id } = req.params;
  taskModel
    .find({ list_id: id })
    .then(doc => {
      if (doc.length != 0) res.status(400).json({ msg: "List is not empty" });
      else {
        listModel
          .findByIdAndDelete(id)
          .then(_ => {
            res.status(200).json({});
          })
          .catch(_ => {
            res.status(500).json({});
          });
      }
    })
    .catch(_ => {
      res.status(500).json({});
    });
}

async function deleteTask(req, res, next) {
  const { id } = req.params;
  taskModel
    .findByIdAndRemove(id)
    .then(_ => {
      console.log(_);
      res.status(200).json({});
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({ error: err });
    });
}

async function addTask(req, res, next) {
  const { title, description, list_id } = req.body;
  var task = new taskModel({
    title,
    description,
    list_id
  });
  try {
    await task.save();
  } catch (error) {
    res.status(500).json({});
  }
  res.json({ msg: "Task added" });
}

async function getTasks(req, res, next) {
  try {
    const result = await taskModel.find(
      {},
      { _id: 1, title: 1, description: 1, completed: 1, list_id: 1 }
    );
    res.json(result);
  } catch (error) {
    res.status(500).json({});
  }
}

async function patchTask(req, res, next) {
  const { id } = req.params;
  try {
    await taskModel.findByIdAndUpdate(id, req.body);
    res.status(200).json({});
  } catch (error) {
    res.status(500).json({});
  }
}

async function getLists(req, res, next) {
  try {
    docs = await listModel.find({}, { _id: 1, title: 1, parent_list: 1 });
    res.status(200).json(docs);
  } catch (err) {
    res.status(500).json({});
  }
}

async function addList(req, res) {
  const { title } = req.body;
  try {
    new listModel({
      title
    }).save();
    res.status(200).json({});
  } catch (err) {
    res.status(500).json({});
  }
}

async function patchList(req, res) {
  const { id } = req.params;
  try {
    listModel.findByIdAndUpdate(id, req.body);
  } catch (err) {
    res.status(500).json({});
  }
}

module.exports = {
  addTask,
  getTasks,
  patchTask,
  deleteTask,
  getLists,
  addList,
  patchList,
  deleteList
};
