const router = require("express").Router();
const {
  addTask,
  getTasks,
  patchTask,
  deleteTask,
  getLists,
  addList,
  patchList,
  deleteList
} = require("./controller");

router.post("/tasks", addTask);
router.get("/tasks", getTasks);
router.patch("/tasks/:id", patchTask);
router.delete("/tasks/:id", deleteTask);
router.get("/lists", getLists);
router.post("/lists", addList);
router.patch("/lists/:id", patchList);
router.delete("/lists/:id", deleteList);

module.exports = router;
