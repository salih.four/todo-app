const express = require("express");
const apiRouter = require("./api/router.js");

app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

app.use("/ui", express.static("./public"));
app.use("/api", apiRouter);

module.exports = app;
