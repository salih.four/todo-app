const mongoose = require("mongoose");

const taskSchema = new mongoose.Schema({
  title: {
    type: String
  },
  description: {
    type: String,
    default: ""
  },
  completed: {
    type: Boolean,
    default: false,
    required: true
  },
  list_id: {
    type: String,
    default: null
  }
});

const listSchema = new mongoose.Schema({
  title: {
    type: String,
    unique: true
  },
  parent_list: {
    type: String,
    required: true,
    default: false
  }
});

const taskModel = mongoose.model("task", taskSchema);
const listModel = mongoose.model("list", listSchema);

module.exports = {
  taskModel,
  listModel
};
