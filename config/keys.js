require("dotenv").config();

const value = {
  PORT: process.env.PORT || 8000,
  MONGO_URI: process.env.MONGO_URI
};

module.exports = value;
