// The current directory should be compiled to /public and then removed before production

import App from "./App.svelte";

var app = new App({
  target: document.body
});

export default app;
